//PokerWallet Script

//Define variables
var playerId= 0
var walletValue = 600
var potValue = 0
var totalBetValue= 0

var playerBets= [0,0,0];

var chips1= 0
var chips5= 0
var chips10= 0
var chips25= 0
var chips100= 0

$serverUri= "http://104.45.228.202:8080"

window.onload = function() {
    hideDiv("votescreen");
    setWalletPot();
    hideDiv("bets");
    showDiv("playerbets");
    getGameValues();
};

(function(){
  const buttons = document.querySelectorAll('.w-button')

  //Add event listeners and functionailty to each button  
  buttons.forEach(function(button){
    button.addEventListener('click', function(){
    if (button.classList.contains('startbutton1')){
        playerId= 1;
        const playerName = document.querySelector('#playername')
        playerName.textContent = "PLAYER "+playerId
        hideDiv("start");
        showDiv("game");
        startGame();
    }
    else if (button.classList.contains('startbutton2')){
        playerId= 2;
        const playerName = document.querySelector('#playername')
        playerName.textContent = "PLAYER "+playerId
        hideDiv("start");
        showDiv("game");
        startGame();
    }
    else if (button.classList.contains('startbutton3')){
        playerId= 3;
        const playerName = document.querySelector('#playername')
        playerName.textContent = "PLAYER "+playerId
        hideDiv("start");
        showDiv("game");
        startGame();
    }
    else if (button.classList.contains('chip1')){
        if (totalBetValue+1<=walletValue) {
            chips1++
            hideDiv("playerbets");
            showDiv("bets");
        }else window.alert("Insufficient funds");
    }
    else if (button.classList.contains('chip5')){
        if (totalBetValue+5<=walletValue) {
            chips5++
            hideDiv("playerbets");
            showDiv("bets");
        }else window.alert("Insufficient funds");

    }
    else if (button.classList.contains('chip10')){
        if (totalBetValue+10<=walletValue) {
            chips10++
            hideDiv("playerbets");
            showDiv("bets");
        }else window.alert("Insufficient funds");
      
    }
    else if (button.classList.contains('chip25')){
        if (totalBetValue+25<=walletValue) {
            chips25++
            hideDiv("playerbets");
            showDiv("bets");
        }else window.alert("Insufficient funds");
      
    }
    else if (button.classList.contains('chip100')){
        if (totalBetValue+100<=walletValue) {
            chips100++
            hideDiv("playerbets");
            showDiv("bets");
        }else window.alert("Insufficient funds");
      
    }
    else if (button.classList.contains('cancel')){
        chips1= 0
        chips5= 0
        chips10= 0
        chips25= 0
        chips100= 0
        hideDiv("bets");
        showDiv("playerbets");  
    }
    else if (button.classList.contains('submit')){
        totalBetValue = chips1+chips5*5+chips10*10+chips25*25+chips100*100
        chips1= 0
        chips5= 0
        chips10= 0
        chips25= 0
        chips100= 0
        walletValue-=totalBetValue;
        potValue+=totalBetValue;
        playerBets[(playerId-1)]+=totalBetValue;
        setWalletPot();
        //updatePlayerBets();
        placeBet(playerId, totalBetValue);
        getGameValues();
        hideDiv("bets");
        showDiv("playerbets");
    }
    else if (button.classList.contains('nextbutton')){
        playerBets= [0,0,0];
        updatePlayerBets();
        nextRound();
    }
    else if (button.classList.contains('stopbutton')){
        hideDiv("game");
        showDiv("votescreen");
    }
    else if (button.classList.contains('vote-p1')){
        placeVote(1, playerId);
        hideDiv("votescreen");
        showDiv("start");
    }
    else if (button.classList.contains('vote-p2')){
        placeVote(2, playerId);
        hideDiv("votescreen");
        showDiv("start");
    }
    else if (button.classList.contains('vote-p3')){
        placeVote(3, playerId);
        hideDiv("votescreen");
        showDiv("start");
    }


    //Calculate current total bet
    totalBetValue = chips1+chips5*5+chips10*10+chips25*25+chips100*100
    //Select the counter text & update
    const totalBet = document.querySelector('#totalbet')
    totalBet.textContent = totalBetValue
    const totalChips1 = document.querySelector('#chip-amount-1')
    totalChips1.textContent = chips1+"x"
    const totalChips5 = document.querySelector('#chip-amount-5')
    totalChips5.textContent = chips5+"x"
    const totalChips10 = document.querySelector('#chip-amount-10')
    totalChips10.textContent = chips10+"x"
    const totalChips25 = document.querySelector('#chip-amount-25')
    totalChips25.textContent = chips25+"x"
    const totalChips100 = document.querySelector('#chip-amount-100')
    totalChips100.textContent = chips100+"x"

    })
  })
})()

function setWalletPot(){
          
    // Set total wallet & pot value
      const totalWallet = document.querySelector('#totalwallet')
      totalWallet.textContent = walletValue
      const totalPot = document.querySelector('#totalpot')
      totalPot.textContent = potValue
      }

function updatePlayerBets(){
    const player1Bets = document.querySelector('#player1-bets')
    player1Bets.textContent = playerBets[0];
    const player2Bets = document.querySelector('#player2-bets')
    player2Bets.textContent = playerBets[1];
    const player3Bets = document.querySelector('#player3-bets')
    player3Bets.textContent = playerBets[2];

}

function showDiv(div) {
    var x = document.getElementById(div);
    x.style.display = "block";

}

function hideDiv(div) {
    var x = document.getElementById(div);
    x.style.display = "none";
    }

// API Functions

function startGame(){
    $.ajax({
        url: $serverUri + '/pokerwallet/start', 
        success: function(data) {
            console.log(data);
        }
    });
}

function getGameValues() {
    $.ajax({
        url: $serverUri + '/pokerwallet/getgamevalues', 
        success: function(data) {
            $('#player1-bets').html(data["totalPlayerBetsRound"][0]);
            $('#player2-bets').html(data["totalPlayerBetsRound"][1]);
            $('#player3-bets').html(data["totalPlayerBetsRound"][2]);
            $('#highestbet').html(data["currentBet"]);
            $('#totalpot').html(data["pot"]);
            console.log(data);
        },
        complete: function() {
            setTimeout(getGameValues, 1000);
        }
    });
}

function placeBet(playerID, totalBetValue){
    $.ajax({
        url: $serverUri + '/pokerwallet/placebet/' + playerID + '/' + totalBetValue, 
        success: function(data) {
            console.log(data);
        }
    });
}

function nextRound(){
    $.ajax({
        url: $serverUri + '/pokerwallet/next', 
        success: function(data) {
            console.log(data);
        }
    });
}

function placeVote(winnerID, playerID){
    $.ajax({
        url: $serverUri + '/pokerwallet/placevote/' + winnerID + '/' + playerID, 
        success: function(data) {
            console.log(data);
        }
    });
}