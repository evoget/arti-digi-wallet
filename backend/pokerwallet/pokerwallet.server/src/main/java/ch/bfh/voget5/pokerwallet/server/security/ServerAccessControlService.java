package ch.bfh.voget5.pokerwallet.server.security;

import java.security.AllPermission;
import java.security.Permissions;

import org.eclipse.scout.rt.platform.Replace;
import org.eclipse.scout.rt.shared.security.RemoteServiceAccessPermission;

import ch.bfh.voget5.pokerwallet.shared.security.AccessControlService;

/**
 * @author evoget
 */
@Replace
public class ServerAccessControlService extends AccessControlService {

	@Override
	protected Permissions execLoadPermissions(String userId) {
		Permissions permissions = new Permissions();
		permissions.add(new RemoteServiceAccessPermission("*.shared.*", "*"));

		// TODO [evoget]: Fill access control service
		permissions.add(new AllPermission());
		return permissions;
	}
}
