package ch.bfh.voget5.pokerwallet.server.game;

public class Game {
	int pot;
	int currentBet;
	int[] totalPlayerBetsRound = new int[] { 0, 0, 0 };
	boolean gameFinished = false;

	public int getPot() {
		return pot;
	}

	public void setPot(int pot) {
		this.pot = pot;
	}

	public int getCurrentBet() {
		return currentBet;
	}

	public void setCurrentBet(int currentBet) {
		this.currentBet = currentBet;
	}

	public int[] getTotalPlayerBetsRound() {
		return totalPlayerBetsRound;
	}

	public void setTotalPlayerBetsRound(int[] totalPlayerBetsRound) {
		this.totalPlayerBetsRound = totalPlayerBetsRound;
	}

	public boolean isGameFinished() {
		return gameFinished;
	}

	public void setGameFinished(boolean gameFinished) {
		this.gameFinished = gameFinished;
	}
}
