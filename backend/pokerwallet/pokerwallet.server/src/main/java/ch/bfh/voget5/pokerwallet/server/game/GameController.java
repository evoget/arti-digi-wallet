package ch.bfh.voget5.pokerwallet.server.game;

import java.util.Set;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.bfh.voget5.pokerwallet.server.blockchain.BlockchainService;

@ApplicationScoped
public class GameController {
	private static final Logger LOG = LoggerFactory.getLogger(BlockchainService.class);
	private Game game;
	Set<Integer> votedPlayers = CollectionUtility.emptyHashSet();

	public void start() {
		game = new Game();
		votedPlayers.clear();
		BEANS.get(BlockchainService.class).initContractConnection();
		BEANS.get(BlockchainService.class).start();
		LOG.info("Game started");
	}

	public void vote(int voteingPlayer, int winningPlayer) {
		if (game == null) {
			start();
		}
		if (votedPlayers.add(voteingPlayer)) {
			BEANS.get(BlockchainService.class).vote(voteingPlayer, winningPlayer);
		}
		game.setGameFinished(CollectionUtility.size(votedPlayers) >= 3);
	}

	public void placebet(int player, int bet) {
		if (game == null) {
			start();
		}
		int[] totalPlayerBetsRound = game.getTotalPlayerBetsRound();
		totalPlayerBetsRound[player - 1] = totalPlayerBetsRound[player - 1] + bet;
		game.setTotalPlayerBetsRound(totalPlayerBetsRound);
		game.setPot(game.getPot() + bet);
		game.setCurrentBet(Math.max(game.getCurrentBet(), bet));

	}

	public Game getgamevalues() {
		if (game == null) {
			start();
		}
		return game;
	}

	public void next() {
		if (game == null) {
			start();
		}
		game.setTotalPlayerBetsRound(new int[] { 0, 0, 0 });
		game.setCurrentBet(0);
	}
}
