package ch.bfh.voget5.pokerwallet.server.blockchain;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.platform.util.ObjectUtility;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.abi.datatypes.Address;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.utils.Convert;

@ApplicationScoped
public class BlockchainService {
	private Web3j web3j;
	List<Address> addressList = CollectionUtility.arrayList(new Address("081714A2257409060fEc47266ca7c0471cEF6325"),
			new Address("813Fb39d8E561774f7B069A7bb553c37C3aEB01C"),
			new Address("224be86105E9105510f4375d7C54BaE69293E85c"));

	private PokerWallet bank;
	List<PokerWallet> playerList;

	private static final Logger LOG = LoggerFactory.getLogger(BlockchainService.class);

	public boolean initContractConnection() {
		if (ObjectUtility.isOneOf(null, web3j, bank)) {
			LOG.info("Connecting to blockchain ...");
			web3j = Web3j.build(new HttpService(String.format("http://%s:%s", "127.0.0.1", 7545)));

			try {
				StaticGasProvider gasProvider = new StaticGasProvider(BigInteger.valueOf(5000),
						BigInteger.valueOf(4_100_000L));
				String contractAddress = getContractAddress("PokerWallet");
				bank = PokerWallet.load(contractAddress, web3j,
						Credentials.create("d2b554bea15b2bec22e41674c8363d378e280e05dd80780f4a7f414606be5c06"),
						gasProvider);
				PokerWallet player1 = PokerWallet.load(contractAddress, web3j,
						Credentials.create("efbd84483a57c521d1338c757dd89a42ee1f461cccdd488dfe7495fd9fc6ac28"),
						gasProvider);
				PokerWallet player2 = PokerWallet.load(contractAddress, web3j,
						Credentials.create("b329ea459d3971302976505ab66c7df3f0fdf712b6d5597ea8888db00b2260c9"),
						gasProvider);
				PokerWallet player3 = PokerWallet.load(contractAddress, web3j,
						Credentials.create("8ec9ee1d6c705d15f009ee2141ac5109ec52f5faf901b3a6f373d150baec897e"),
						gasProvider);

				playerList = CollectionUtility.arrayList(player1, player2, player3);
				LOG.info("Connected");

			} catch (Exception e) {
				LOG.error(e.getMessage());
				return false;
			}
		}
		return true;
	}

	private String getContractAddress(String contractName) throws IOException, URISyntaxException {
		List<String> allLines = Files.readAllLines(Paths.get(
				BlockchainService.class.getResource("/pokerwallet/build/contracts/" + contractName + ".json").toURI()));
		String jsonString = StringUtility.join("", allLines);
		JSONObject obj = new JSONObject(jsonString);
		return obj.getJSONObject("networks").getJSONObject("5777").getString("address");
	}

	public boolean start() {
		initContractConnection();
		BigInteger buyIn = Convert.toWei("1.0", Convert.Unit.ETHER).toBigInteger();
		try {
			bank.startGame().send();

			int i = 0;
			for (PokerWallet player : playerList) {
				player.addPlayer(addressList.get(i).getValue(), buyIn).send();
				i++;
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return false;
	}

	public void vote(Integer voteingPlayer, Integer winningPlayer) {
		PokerWallet votingPlayer = playerList.get(voteingPlayer - 1);
		Address winnerAccount = addressList.get(winningPlayer - 1);
		try {
			votingPlayer.vote(winnerAccount.getValue()).send();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
}
