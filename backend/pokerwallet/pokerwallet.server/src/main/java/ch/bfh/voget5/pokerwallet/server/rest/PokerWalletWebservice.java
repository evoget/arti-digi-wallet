package ch.bfh.voget5.pokerwallet.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.scout.rt.platform.BEANS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.bfh.voget5.pokerwallet.server.blockchain.BlockchainService;
import ch.bfh.voget5.pokerwallet.server.game.GameController;

@Path("pokerwallet")
public class PokerWalletWebservice {
	private static final Logger LOG = LoggerFactory.getLogger(BlockchainService.class);
	private ObjectMapper mapper = new ObjectMapper();
	Response.ResponseBuilder rb;

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("start")
	public Response start() {
		BEANS.get(GameController.class).start();
		LOG.info("Game started");
		return getResponseWithHeader("Game started");
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("placevote/{winningPlayer : \\d+}/{voteingPlayer : \\d+}")
	public Response placevote(@PathParam("voteingPlayer") Integer voteingPlayer,
			@PathParam("winningPlayer") Integer winningPlayer) {
		BEANS.get(GameController.class).vote(voteingPlayer, winningPlayer);
		LOG.info("Vote placed for Winner {} from Player {} registered", winningPlayer, voteingPlayer);

		return getResponseWithHeader(
				"Vote placed for Winner " + winningPlayer + " from Player " + voteingPlayer + " registered");
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("placebet/{player : \\d+}/{bet : \\d+}")
	public Response placebet(@PathParam("player") Integer player, @PathParam("bet") Integer bet) {
		BEANS.get(GameController.class).placebet(player, bet);
		LOG.info("Bet with value={} placed for player {}", bet, player);
		return getResponseWithHeader("Bet with value=" + bet + " placed for player " + player);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("getgamevalues")
	public Response getgamevalues(@Context UriInfo u) {
		String response;
		try {
			response = mapper.writeValueAsString(BEANS.get(GameController.class).getgamevalues());
		} catch (JsonProcessingException e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
		return getResponseWithHeader(response);
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("next")
	public Response next(@Context UriInfo u) {
		BEANS.get(GameController.class).next();
		return getResponseWithHeader("New betting-round started");
	}

	private Response getResponseWithHeader(String msg) {
		return Response.ok(msg).header("Access-Control-Allow-Origin", "*").build();
	}
}
