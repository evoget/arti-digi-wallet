pragma solidity >=0.4.21 <0.7.0;

contract PokerWallet{
  event Deposit(address indexed _from, address indexed _id, uint _value);
  event PlayerVoted(address player, address winner, uint currentVotes);
  event Winner(address player);

  uint pot = 0;
  address payable[] private players;

  mapping(address => bool) public voters;
  mapping(address => uint) public voteCount;

  function vote(address payable player) public returns (bool){
    if (voters[msg.sender]){
      return false;
    }
    voters[msg.sender] = true;
    voteCount[player] = voteCount[player]+1;
    emit PlayerVoted(msg.sender, player, voteCount[player]);
    if (voteCount[player] > 1){
      player.transfer(pot);
      emit Winner(player);
      return true;
    }
    return false;
  }

  function startGame() public returns (bool){
    for (uint i = 0; i < players.length; i++) {
        voteCount[players[i]]=0;
        voters[players[i]]=false;
    }
    delete players;
    pot = 0;
  }

  function addPlayer(address player) public payable returns (bool){
    if (msg.value >= 1){
      players.push(msg.sender);
      voteCount[player] = 0;
      pot += msg.value;
    }
    emit Deposit(msg.sender, player, msg.value);
  }
}
